import { Hero } from "src/hero";

export const HEROES: Hero[] = [
    {id: 12, name: 'Dr. Bagus'},
    {id: 13, name: 'Bombastis'},
    {id: 14, name: 'Celebrities'},
    {id: 15, name: 'Magnitudo'},
    {id: 16, name: 'Roberto'},
    {id: 17, name: 'Dynamo'},
    {id: 18, name: 'Dr. ESQ'},
    {id: 19, name: 'Wedus'},
    {id: 20, name: 'Puting Beliung'}
]