import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from 'src/hero';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDataService{
  createDb() {
    const heroes = [
      {id:12, name: 'Dr. Baik'},
      {id:13, name: 'Bombastis'},
      {id:14, name: 'Celebrity'},
      {id:15, name: 'Magneton'},
      {id:16, name: 'RubberHand'},
      {id:17, name: 'Dynamo'},
      {id:18, name: 'Dr. ESQ'},
      {id:19, name: 'Wedus'},
      {id:20, name: 'Puting Beliung'}
    ];
    return {heroes};
  }

  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id))+1 : 11;
  }
}
